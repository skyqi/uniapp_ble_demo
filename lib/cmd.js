// 所有指令处理

import maxhomapi from '@/lib/maxhom.js'
import bleApiAll from '@/lib/ble.js'

const maxhom = maxhomapi.maxhom;
const ble = bleApiAll.ble;

let pthis;
const cmd = {
	head:'',
	end:'BB',
	ctrlLed(data){
		//长条LED灯控制
		let cmd = 'AA'+data.number+data.speed+data.num+data.color;
		this.send(cmd);
	},
	ctrlled2(data){
		//环形LED灯控制
		let cmd = 'CC'+data.number+data.color+data.time;
		this.send(cmd);
	},
	send(cmd){
		//发送指令
		var newCmd = cmd+this.end;
		
	  var mac = getApp().globalData.mac[0];
	  if(!mac){
		maxhom.toast('当前未连接设备');
		return;
	  }
	  ble.sendCmd(mac,newCmd);
	}  
}
module.exports = {
  cmd
}