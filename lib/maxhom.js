
let maxhom = {
  getTime(dateType){
		var date = new Date();
		var year = date.getFullYear();
		var mouth = date.getMonth()+1;
		var dates = date.getDate();
		var day = date.getDay();
			
			day = day==0?7:day;
			
		var hour = date.getHours();
		var min = date.getMinutes();
		var sec = date.getSeconds();
			year = this.hex16(String(year).substr(-2));
			mouth = this.hex16(mouth);
			dates = this.hex16(dates);
			day = this.hex16(day);
			hour = this.hex16(hour);
			min = this.hex16(min);
			sec = this.hex16(sec);
		
		if(dateType == 1){
			return year+mouth+dates+hour+min+sec;
		}
		return year+mouth+dates+hour+min+sec+day;
  },
  hex16(str){
		return this.two((Number(str)).toString(16));
  },
	hex10(num){
		if(num == undefined){
			return '00';
		}
		return this.two(parseInt(num,16));	
	},
	hex2(str){
		return this.buwei(parseInt(str,16).toString(2),8);
	},
	buwei(val,num){
		//补位
		var str1 =  new Array(num-val.length+1).join("0")+val;		
		return str1.substr(0,num);
	},
	two(val){
		if(val){
			val = String(val);	
			return val.length<2?"0"+val:val;
		}else{
			return '00';
		}
	},
	erTohex16(val){
		//二进制转16进制
		return this.hex16(parseInt(val,2))
	},
	toast(msg){
		uni.showToast({
		  title: msg,
		  icon: 'none',
		  duration: 3000
		})
	  },
	gotoPage(url){
		uni.navigateTo({
			url:url
		})
	},  
}

module.exports = {
  maxhom
}