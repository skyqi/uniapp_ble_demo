
const ble = {
  readUUID1:'',//兼容多连设备处理
  readUUID2:'',//兼容多连设备处理
  writeUUID1:'',//兼容多连设备处理
  writeUUID2:'',//兼容多连设备处理
  batteryUUID1:'',//兼容多连设备处理
  batteryUUID2:'',//兼容多连设备处理
  curType:1,//当前连接的是哪个设备
  readBattery:false,//是否读电池
  deviceTimer:'',//获取列表
  curConnect:'',//当前连接状态
  autoArrRssi:{},//保存设备的信号值
  serviceUUID:[{//根据设备的uuid添加 
    // readUUID1:'0000FFF0-0000-1000-8000-00805F9B34FB',
    // readUUID2:'0000FFF1-0000-1000-8000-00805F9B34FB',
    // writeUUID1:'0000FFF0-0000-1000-8000-00805F9B34FB',
    // writeUUID2:'0000FFF2-0000-1000-8000-00805F9B34FB',
	readUUID1:'6E400001-B5A3-F393-E0A9-E50E24DCCA9E',
	readUUID2:'6E400003-B5A3-F393-E0A9-E50E24DCCA9E',
	writeUUID1:'6E400001-B5A3-F393-E0A9-E50E24DCCA9E',
	writeUUID2:'6E400002-B5A3-F393-E0A9-E50E24DCCA9E',
  }],
  getUUID(){
    // var sysVal = api.systemType;
	//根据this.curType 变化uuid 考虑设备多连
	this.readUUID1 = this['serviceUUID'][this.curType-1].readUUID1;
	this.readUUID2 = this['serviceUUID'][this.curType-1].readUUID2;
	this.writeUUID1 = this['serviceUUID'][this.curType-1].writeUUID1;
	this.writeUUID2 = this['serviceUUID'][this.curType-1].writeUUID2
    						
  },
  init(){			
    bleApi.init();
  },
  scan(callback){
    //扫描
    bleApi.scan(callback);
  },
  getList(){
    var _this = this;
	console.log('执行了？')
    clearInterval(this.deviceTimer);
    this.deviceTimer = setInterval(function(){
      bleApi.getList(function(res){
		  console.log(res)
        // _this.upList(call);
        _this.saveRssi(res);//判断设备是否在搜索范围
        // callback&&callback(res);
		_this.recvList(res);//将数据传到页面
      });
    },3000);
  },
  saveRssi(res){
    //根据RSSI判断当前设备是否在搜索范围  
	//RSSI有变化时，设备在广播。没变化时，设备不在搜索范围或者断电了
	//可以根据RSSI值重连设备
    if(res.devices){
      for(let i=0;i<res.devices.length;i++){
        if(res.devices[i].localName&&this.glDevice(res.devices[i].localName)){
          var name = res.devices[i].localName;
          var id = res.devices[i].deviceId;
          var rssi = res.devices[i].RSSI;
          if(!this.autoArrRssi[id]){
            //没有保存过		   
            this.autoArrRssi[id] = {};
            this.autoArrRssi[id].rssi = rssi;
            this.autoArrRssi[id].num = 0;//累计3次不一样的话，直接清空
          }else{
            // console.log('RSSI',id,bleApi.mac,this.autoArrRssi[id].num,rssi)
            if(id != bleApi.mac){
              //连接的设备RSSI是不变的，不处理
              if(rssi == this.autoArrRssi[id].rssi){
                //一样的话 清零					
                this.autoArrRssi[id].num = 0;
                
              }else{
                //不一样的话清0 保留上次rssi
               
                this.autoArrRssi[id].num++;
                if(this.autoArrRssi[id].num >=2){
                  //直接连接  (重连处理)
				  //不能直接连接。需要连接过的设备
				   
				  if(id == getApp().globalData.mac[0].mac){
					  this.connect(id,name);
					  //连接的时候清空下数据
					  this.autoArrRssi[id].num = 0;
				  }
                }
              }
            }
            this.autoArrRssi[id].rssi = rssi;
          }
        }
      }
    }
  },
  getDeviceType(name){
    //获取设备类型  name 
    return 1;
  },
  glDevice(name){
    //过滤当前搜索时的设备名字
    if(name){

      return true;
    }
    return false;
    
  },  
  deviceStatus(callback){
    //设备连接状态
    bleApi.lisenDeivce(callback); 
  }, 
  disConnect(mac){
	  //断开设备
	  console.log('断开设备',mac)
	  bleApi.discontent(mac);
  },
  connect(mac,name){
    //开始连接设备
    var _this = this;
    console.log(mac,name)
    this.curType = this.getDeviceType(name);//判断当前是什么设备
    this.getUUID();//获取设备服务
    this.curConnect = true; //标志连接状态		
	console.log('开始',new Date().getTime());
    bleApi.name = name;//保存名字

    getApp().globalData.mac[0].name = name;

    bleApi.connect(mac,function(ret){
      console.log(ret)
      if(ret.errMsg == 'createBLEConnection:ok'){
        bleApi.step1(mac);
      }else{
        //连接失败
        _this.curConnect = false;
        // bleApi.showLoading({status:true});
        bleApi.toast('连接失败');
        _this.connectError(mac,name);
      }
    });
  },
  connectError(mac,name){
    //连接错误
    console.log('连接失败',mac,name)
    bleApi.mac = '';
    bleApi.name = '';
  },
  connected(mac){    
    bleApi.toast('设备连接成功');

    var _this = this;
    bleApi.mac = mac;

    getApp().globalData.mac[0].mac = mac;

    bleApi.subscribeble(mac,function(ret){
      console.log('侦听',ret)
        // _this.recvFun();
		console.log('结束',new Date().getTime());
      if(ret.status){
		_this.recvFun();
      }
    });
    //连接后处理其它
    this.connectedStyle();
  },
  connectedStyle(){
    //跳转到首页

    var pageArr = getCurrentPages();
    
    if(pageArr[pageArr.length-1].route != 'pages/main/main'){
      uni.navigateTo({
        url: '../main/main',
      })
    }else{
      //重新加载界面
      pageArr[pageArr.length-1].onLoad();
    }
    
  },
  sendCmd(mac,cmd){
    //写数据 这里多连。mac传的是数组Json  [{mac:xxx,name:xxx}]
    
    var sendIndex = 0;//this.getMacIndex(mac);
    bleApi.sendcommod(mac.mac,this.getbuffer(cmd),function(ret){
      console.log('写入'+JSON.stringify(ret),cmd);
	  
    },this.serviceUUID[sendIndex].writeUUID1,this.serviceUUID[sendIndex].writeUUID2);
  },
  recvList(res){
	  //页面创建getbleList方法，搜索到设备时会传到界面
		
	var pageArr = getCurrentPages();
	  
	pageArr[pageArr.length-1].$vm.recvbleList&&pageArr[pageArr.length-1].$vm.recvbleList(res);
	
	
  },
  recvData(res){
	//数据处理
	//页面创建recvbleData方法，搜索到设备时会传到界面
    var pageArr = getCurrentPages();
	
	pageArr[pageArr.length-1].$vm.recvbleData&&pageArr[pageArr.length-1].$vm.recvbleData(res); 
  },
  recvFun(callback){
    //数据处理
    var _this = this;
    uni.onBLECharacteristicValueChange(function (res) {
      // console.log("接收的值", _this.gethex(res.value));
      var data = _this.gethex(res.value);
  
      if (data) {
		_this.recvData({
			mac:res.deviceId,
			data:data
		});
        // callback && callback({
        //   mac:res.deviceId,
        //   data:data
        // });
      }
    });
  },
  getbuffer: function (hex) {
    var typedArray = new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16)
    }))

    var buffer = typedArray.buffer;
    return buffer;
  },
  
  gethex: function (buffer) {
    var hexArr = Array.prototype.map.call(
      new Uint8Array(buffer),
      function (bit) {
        return ('00' + bit.toString(16)).slice(-2)
      }
    )
    return hexArr.join('');
  }
}


const bleApi = {
  mac:'',//当前连接的mac
  name:'',//当前连接的name
  init:function(){  
    var _this = this;
    console.log('初始化')
    this.open(function(res){
      //监听蓝牙状态
      // console.log(res)
      if(res.errMsg == 'openBluetoothAdapter:ok'){
        _this.scan();
      }else{
		  
	  }
      _this.lisen();
      // _this.lisenDeivce();
    });
  },
  open:function(callback){
    //初始化模块
    var _this = this;
    uni.openBluetoothAdapter({
      success (res) {
        callback&&callback(res)
      },
      fail(res){
        callback&&callback({status:false});
        _this.toast('当前手机未开启蓝牙，请打开手机蓝牙（已开启忽略此消息）');
        
      }
    });
  },
  lisen:function(){
    //监听手机蓝牙状态
    var _this = this;
    uni.onBluetoothAdapterStateChange(function (res) {
      console.log('adapterState changed, now is', res)
      // console.log(res)
      // ble.status(res);
      if(res.available&&!res.discovering){
        //如果蓝牙打开并且不在搜索状态。重新扫描
		console.log('开始扫描')
        _this.scan(res.discovering);
      }
    });  
  },
  lisenDeivce:function(callback){
    //监听设备连接状态
    var _this = this;
    uni.onBLEConnectionStateChange(function(res) {
      // 该方法回调中可以用于处理连接意外断开等异常情况      
      // ble.deviceStatus(res)
      
      if(res.deviceId == getApp().globalData.mac[0].mac){
        getApp().globalData.mac[0] = {};
        _this.mac = '';
        _this.name = '';
        console.log(_this.mac,_this.name)
      }
      callback&&callback(res);
    });
  },
  scan:function(isStop,callback){
    //扫描设备 先停止扫描
    isStop&&this.stopScan();
	var _this = this;
    uni.startBluetoothDevicesDiscovery({
      // services: [ble.serviceUUID[0].readUUID1,ble.serviceUUID[1].readUUID1,ble.serviceUUID[2].readUUID1],
      allowDuplicatesKey: true,//根据rssi重连
      success (res) {		   
        callback&&callback(res);
		clearInterval(ble.deviceTimer);
        ble.getList();
      },
      fail(res){
        callback&&callback({status:false});
		if(res.errMsg == 'startBluetoothDevicesDiscovery:fail not init'){
			_this.init();
		}
      }
    });
  },
  stopScan:function(){
    //停止扫描设备
    uni.stopBluetoothDevicesDiscovery({
      success (res) {
        console.log('停止扫描'+res)
      }
    })
  },
  getList:function(callback){
    //获取设备
    uni.getBluetoothDevices({
      success (res) {
        callback&&callback(res);
      },
      fail(res){
        callback&&callback({status:false,devcices:[]});
      }
    });
  },
  connect:function(mac,callback){
    //连接设备
    this.showLoading({msg:'设备开始连接中'});
    uni.createBLEConnection({
      deviceId:mac,
      success (res) {		 
        callback&&callback(res);
      },
      fail(res){
        callback&&callback({status:false});
      }
    })
  },
  getFwuuid:function(mac,callback){
    //获取服务ID
    uni.getBLEDeviceServices({
      // 这里的 deviceId 需要已经通过 uni.createBLEConnection 与对应设备建立连接
      deviceId:mac,
      success (res) {
        console.log(mac,'服务',res)
        var ret = {
          status:res.errMsg == 'getBLEDeviceServices:ok'?true:false
        }
        callback&&callback(ret);
      },
      fail(res){
        
        callback&&callback({status:false});
      }
    })    
  },
  getTzuuid:function(mac,uuid,callback){
    //获取特征ID
    // console.log('><<'+uuid)
    uni.getBLEDeviceCharacteristics({
      // 这里的 deviceId 需要已经通过 uni.createBLEConnection 与对应设备建立链接
      deviceId:mac,
      // 这里的 serviceId 需要在 uni.getBLEDeviceServices 接口中获取
      serviceId:uuid,
      success (res) {
        // console.log('特征',res)
        var ret = {
          status:res.errMsg == 'getBLEDeviceCharacteristics:ok'?true:false
        }
        callback&&callback(ret);
      },
      fail(res){
        callback&&callback({status:false});
      }
    });
  },
  sendcommod:function(mac,cmd,callback,uuid1,uuid2){
    //写数据
    // console.log(mac,cmd,uuid1,uuid2)
    uni.writeBLECharacteristicValue({
      // 这里的 deviceId 需要在 getBluetoothDevices 或 onBluetoothDeviceFound 接口中获取
      deviceId:mac,
      // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
      serviceId:uuid1||ble.writeUUID1,
      // 这里的 characteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
      characteristicId:uuid2||ble.writeUUID2,
      // 这里的value是ArrayBuffer类型
      value: cmd,
      success (res) {
        callback&&callback(res);
      },
      fail(res){
        callback&&callback({status:false,msg:res.errMsg});
      }
    });
  },
  subscribeble:function(mac,callback){
    //侦听数据
    uni.notifyBLECharacteristicValueChange({
      state: true, // 启用 notify 功能
      // 这里的 deviceId 需要已经通过 createBLEConnection 与对应设备建立链接
      deviceId:mac,
      // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
      serviceId:ble.readUUID1,
      // 这里的 characteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
      characteristicId:ble.readUUID2,
      success (res) {
        callback&&callback(res.errMsg=='notifyBLECharacteristicValueChange:ok'?{status:true}:{status:false});
      },
      fail(res){
        callback&&callback({status:false});
      }
    });
    
  },
  discontent:function(mac,callback){
    //断开设备
    uni.closeBLEConnection({
      deviceId:mac,
      success (res) {
        callback&&callback(res);
      },
      fail(res){
        callback&&callback({status:false});
      }
    })    
  },
  getRssi:function(mac,callback){    
    uni.getBLEDeviceRSSI({
      deviceId:mac,
      success(res){
        callback&&callback(res);
      },fail(res){
        callback&&callback({status:false});
      }
    })
  },
  readcommod:function(mac,uuid1,uuid2,callback){
    //主动读取设备数据
    uni.readBLECharacteristicValue({
      // 这里的 deviceId 需要已经通过 createBLEConnection 与对应设备建立链接
      deviceId:mac,
      // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
      serviceId:uuid1,
      // 这里的 characteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
      characteristicId:uuid2,
      success (res) {
        callback&&callback(res);
      },
      fail(res){
        callback&&callback({status:false});
      }
    })
  },
  showLoading(data){
    if(data.status){
      uni.hideLoading();
    }else{
      uni.showLoading({
        title: data.msg,
        mask:false
      });
    }
  },
  toast(msg){
    uni.showToast({
      title: msg,
      icon: 'none',
      duration: 3000
    })
  },
  step1:function(mac){
    //连接设备的第一步
    var _this = this;
    this.mac = mac;
    // dialog.showLoading(true);
    _this.showLoading({msg:'设备连接中'});
    setTimeout(function(){
		_this.getFwuuid(mac,function(ret,err){
		  _this.showLoading({status:true});
		  if(ret.status){
		    //获取成功进入step2
		    _this.step2(mac);
		  }else{
		    //失败
		
		  }
		});
		//这里需要延时2s获取getFwuuid  1秒连接不稳定
	},2000);
  },
  step2:function(mac){
    //连接设备的第二步
    var _this = this;
    //重连uuid另外处理

    if(ble.readUUID1 != ble.writeUUID1){
      //服务ID不一样  打开两次
      _this.showLoading({msg:'获取Services'});
      this.getTzuuid(mac,ble.readUUID1,function(ret,err){
        if(ret.status){
          _this.showLoading({status:true});
          setTimeout(function(){
            _this.showLoading({msg:'获取Characteristics'});
            _this.getTzuuid(mac,ble.writeUUID1,function(ret1,err1){
              _this.showLoading({status:true});
              if(ret1.status){									
                if(ble.readBattery){
                  //读取的话再打开一次  延迟500ms打开 否则会打开错误
                  setTimeout(function(){
                    _this.showLoading({msg:'获取Characteristics'});
                    _this.getTzuuid(mac,ble.batteryUUID1,function(ret2,err2){
                      _this.showLoading({status:true});
                      if(ret2.status){
                        ble.connected(mac);//连接成功
                      }else{
                        console.log('打开读取电量特征ID错误');
                      }
                    });
                  },500);
                }else{
                  ble.connected(mac);//连接成功
                }
              }else{
                console.log('第一次打开特征ID错误');
              }
            });
          },500);

        }else{
          _this.showLoading({status:true});
          console.log('第一次打开特征ID错误');
        }
      });

    }else{
      //一样的话，打开一次
      _this.showLoading({msg:'获取Services'});
	  
      this.getTzuuid(mac,ble.readUUID1,function(ret,err){
        //
        _this.showLoading({status:true});
        if(ret.status){
          //打开成功  判读是否需要读取电量
          if(ble.readBattery){
            //读取的话再打开一次  延迟500ms打开 否则会打开错误
            setTimeout(function(){
              _this.showLoading({msg:'获取Characteristics'});
              _this.getTzuuid(mac,ble.batteryUUID1,function(ret1,err1){
                _this.showLoading({status:true});
                if(ret1.status){
                  ble.connected(mac);//连接成功
                }else{
                  console.log('打开读取电量特征ID错误');
                }
              });
            },500);
          }else{
            ble.connected(mac);//连接成功
          }
        }else{
          console.log('第一次打开特征ID错误');
        }
      });
    }

  }

}

module.exports = {
  ble,
  bleApi
}