import App from './App'

import ble from './lib/ble'
import maxhom from './lib/maxhom'
import cmd from './lib/cmd'

// ble封装
Vue.prototype.$ble = ble.ble

// 公共方法
Vue.prototype.$maxhom = maxhom.maxhom

// 指令集合处理
Vue.prototype.$cmd = cmd.cmd

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif